## Description

Proxy Clients & Routing Rules.

## Apps

### Android

[CloudflareWarp](https://apk.support/download-app/com.cloudflare.onedotonedotonedotone)	[SurfVPN](https://apk.support/download-app/com.xfx.surfvpn)	[USAvpn](https://apk.support/download-app/free.vpnusa.fast.unlimited.free.secure.turbo)	

v2ray：https://github.com/2dust/v2rayNG/releases

### Windows

[CloudflareWarp](https://install.appcenter.ms/orgs/cloudflare/apps/1.1.1.1-windows-1/distribution_groups/release)	WarpIP/ IP优选

v2ray：https://github.com/2dust/v2rayN/releases/

### Routing Rules

Foreign Proxy：

​	block：geosite:category-ads-all,

​	direct：geosite:cn,	geoip:cn,geoip:private	

​	proxy：0-65535

Custom Proxy：

​	block：geosite:category-ads-all,

​	proxy：proxylist

​	direct：0-65535

## References
Proxy Rules:

https://github.com/blackmatrix7/ios_rule_script

GFWlists:

https://github.com/ACL4SSR/ACL4SSR/tree/master

https://raw.githubusercontent.com/gfwlist/gfwlist/master/gfwlist.txt

https://raw.githubusercontent.com/Loukky/gfwlist-by-loukky/master/gfwlist.txt

[GFWlist - Base64 Decode](https://www.base64decode.org/)	

